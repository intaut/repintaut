package com.example.lab.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class Alfabetizacao extends AppCompatActivity implements View.OnClickListener {

    private ImageButton imgButton2;
    private Button btn5, btn6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alfabetizacao);
        imgButton2 = (ImageButton) findViewById(R.id.imageButton2);
        imgButton2.setOnClickListener(this);
        btn5 = (Button) findViewById(R.id.button5);
        btn5.setOnClickListener(this);
        btn6 = (Button) findViewById(R.id.button6);
        btn6.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == imgButton2) {
            Intent i = new Intent(this, Principal.class);
            startActivity(i);
        }
        //Redirecionar PORTUGUES
        if (view == btn5) {
            Intent i = new Intent(this, Portugues.class);
            startActivity(i);
        }
        //Redirecionar MATEMATICA
        if (view == btn6) {
            Intent i = new Intent(this, Matematica.class);
            startActivity(i);
        }
    }
}
