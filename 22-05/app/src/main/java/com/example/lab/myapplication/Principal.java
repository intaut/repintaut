package com.example.lab.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class Principal extends AppCompatActivity implements View.OnClickListener {

    private ImageButton imgButton;
    private Button btn1, btn2, btn3, btn4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        imgButton = (ImageButton) findViewById(R.id.imageButton);
        imgButton.setOnClickListener(this);
        btn1 = (Button) findViewById(R.id.button1);
        btn1.setOnClickListener(this);
        btn2 = (Button) findViewById(R.id.button2);
        btn2.setOnClickListener(this);
        btn3 = (Button) findViewById(R.id.button3);
        btn3.setOnClickListener(this);
        btn4 = (Button) findViewById(R.id.button4);
        btn4.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == imgButton) {
            Intent i = new Intent(this, Inicial.class);
            startActivity(i);
        }
        //Redirecionar EMOÇÕES
        if (view == btn1) {
            Intent i = new Intent(this, Principal.class);
            startActivity(i);
        }
        //Redirecionar AÇÕES
        if (view == btn2) {
            Intent i = new Intent(this, Principal.class);
            startActivity(i);
        }
        //Redirecionar CONCENTRAÇÃO
        if (view == btn3) {
            Intent i = new Intent(this, Principal.class);
            startActivity(i);
        }
        //Redirecionar ALFABETIZAÇÃO
        if (view == btn4) {
            Intent i = new Intent(this, Alfabetizacao.class);
            startActivity(i);
        }
    }
}
