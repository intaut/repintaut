package com.example.lab.myapplication;


public class ApoioMatematica {
    private int imagemMatematica;
    private String textoImagemMatematica;

    public int getImagemMatematica() {

        return imagemMatematica;
    }

    public void setImagemMatematica(int imagemMatematica) {
        this.imagemMatematica = imagemMatematica;
    }

    public String getTextoImagemMatematica() {

        return textoImagemMatematica;
    }

    public void setTextoImagemMatematica(String textoImagemMatematica) {
        this.textoImagemMatematica = textoImagemMatematica;
    }
}

