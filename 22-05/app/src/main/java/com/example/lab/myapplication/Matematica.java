package com.example.lab.myapplication;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Matematica extends AppCompatActivity implements View.OnClickListener {
    private ImageView img;
    private Button btn1, btn2, btn3;
    private ImageButton btnTrocar;
    private ArrayList<ApoioMatematica> lista;
    private int itemCerto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matematica);

        img = (ImageView) findViewById(R.id.imageView2);

        btnTrocar = (ImageButton) findViewById(R.id.imageButton3);
        btnTrocar.setOnClickListener(this);

        btn1 = (Button) findViewById(R.id.button12);
        btn1.setOnClickListener(this);

        btn2 = (Button) findViewById(R.id.button11);
        btn2.setOnClickListener(this);

        btn3 = (Button) findViewById(R.id.button10);
        btn3.setOnClickListener(this);

        carregarElementos();

        iniciarJogo();
    }

    public void iniciarJogo(){

        btn1.setBackgroundColor(Color.parseColor("#7cb342"));
        btn2.setBackgroundColor(Color.parseColor("#7cb342"));
        btn3.setBackgroundColor(Color.parseColor("#7cb342"));

        Collections.shuffle(lista); /* para embaralhar a lista */

        img.setImageResource(lista.get(0).getImagemMatematica()); /* setar imagem */

        Random r = new Random();
        int posicao = r.nextInt(3); /* gerar inteiro até 3 */

        switch(posicao ){
            case 0:
                itemCerto = 1;
                btn1.setText(lista.get(0).getTextoImagemMatematica());
                btn2.setText(lista.get(1).getTextoImagemMatematica());
                btn3.setText(lista.get(2).getTextoImagemMatematica());
                break;

            case 1:
                itemCerto = 2;
                btn2.setText(lista.get(0).getTextoImagemMatematica());
                btn1.setText(lista.get(1).getTextoImagemMatematica());
                btn3.setText(lista.get(2).getTextoImagemMatematica());
                break;

            case 2:
                itemCerto = 3;
                btn3.setText(lista.get(0).getTextoImagemMatematica());
                btn2.setText(lista.get(1).getTextoImagemMatematica());
                btn1.setText(lista.get(2).getTextoImagemMatematica());
                break;
        }

    }
    public void carregarElementos() {
        lista = new ArrayList<ApoioMatematica>();

        ApoioMatematica a = new ApoioMatematica();
        a.setImagemMatematica(R.drawable.um);
        a.setTextoImagemMatematica("1");
        lista.add(a);

        ApoioMatematica b = new ApoioMatematica();
        b.setImagemMatematica(R.drawable.dois);
        b.setTextoImagemMatematica("2");
        lista.add(b);

        ApoioMatematica c = new ApoioMatematica();
        c.setImagemMatematica(R.drawable.tres);
        c.setTextoImagemMatematica("3");
        lista.add(c);

        ApoioMatematica d = new ApoioMatematica();
        d.setImagemMatematica(R.drawable.quatro);
        d.setTextoImagemMatematica("4");
        lista.add(d);

        ApoioMatematica e = new ApoioMatematica();
        e.setImagemMatematica(R.drawable.cinco);
        e.setTextoImagemMatematica("5");
        lista.add(e);

        ApoioMatematica f = new ApoioMatematica();
        f.setImagemMatematica(R.drawable.seis);
        f.setTextoImagemMatematica("6");
        lista.add(f);

        ApoioMatematica g = new ApoioMatematica();
        g.setImagemMatematica(R.drawable.sete);
        g.setTextoImagemMatematica("7");
        lista.add(g);

        ApoioMatematica h = new ApoioMatematica();
        h.setImagemMatematica(R.drawable.oito);
        h.setTextoImagemMatematica("8");
        lista.add(h);

        ApoioMatematica i = new ApoioMatematica();
        i.setImagemMatematica(R.drawable.nove);
        i.setTextoImagemMatematica("9");
        lista.add(i);

        ApoioMatematica j = new ApoioMatematica();
        j.setImagemMatematica(R.drawable.dez);
        j.setTextoImagemMatematica("10");
        lista.add(j);

        ApoioMatematica k = new ApoioMatematica();
        k.setImagemMatematica(R.drawable.onze);
        k.setTextoImagemMatematica("11");
        lista.add(k);

        ApoioMatematica l = new ApoioMatematica();
        l.setImagemMatematica(R.drawable.doze);
        l.setTextoImagemMatematica("12");
        lista.add(l);

        ApoioMatematica m = new ApoioMatematica();
        m.setImagemMatematica(R.drawable.treze);
        m.setTextoImagemMatematica("13");
        lista.add(m);

        ApoioMatematica n = new ApoioMatematica();
        n.setImagemMatematica(R.drawable.quatorze);
        n.setTextoImagemMatematica("14");
        lista.add(n);

        ApoioMatematica o = new ApoioMatematica();
        o.setImagemMatematica(R.drawable.quinze);
        o.setTextoImagemMatematica("15");
        lista.add(o);

        ApoioMatematica p = new ApoioMatematica();
        p.setImagemMatematica(R.drawable.dezesseis);
        p.setTextoImagemMatematica("16");
        lista.add(p);


    }

    @Override
    public void onClick(View view) {
        if(view == btnTrocar){
            iniciarJogo();
        }
        else if(view == btn1){
            if(itemCerto == 1){
                btn1.setBackgroundColor(Color.GREEN);
                iniciarJogo();
            }
            else{
                btn1.setBackgroundColor(Color.RED);
            }
        }
        else if(view == btn2){
            if(itemCerto == 2){
                btn2.setBackgroundColor(Color.GREEN);
                iniciarJogo();
            }
            else{
                btn2.setBackgroundColor(Color.RED);
            }
        }
        else if(view == btn3){
            if(itemCerto == 3){
                btn3.setBackgroundColor(Color.GREEN);
                iniciarJogo();
            }
            else{
                btn3.setBackgroundColor(Color.RED);
            }
        }
    }
}
