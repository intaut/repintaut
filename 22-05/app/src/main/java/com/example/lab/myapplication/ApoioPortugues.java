package com.example.lab.myapplication;


public class ApoioPortugues {
    private int imagemPortugues;
    private String textoImagemPortugues;

    public int getImagemPortugues() {
        return imagemPortugues;
    }

    public void setImagemPortugues(int imagemPortugues) {
        this.imagemPortugues = imagemPortugues;
    }

    public String getTextoImagemPortugues() {
        return textoImagemPortugues;
    }

    public void setTextoImagemPortugues(String textoImagem) {
        this.textoImagemPortugues = textoImagem;
    }
}

