package com.example.lab.myapplication;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.provider.MediaStore;
import java.util.ArrayList;
import java.util.Collections;
import android.content.pm.PackageManager;
import java.util.Random;

public class Camera extends AppCompatActivity implements View.OnClickListener {
    private ImageView imgComparacao, imgImagem;
    private ImageButton btnTrocar, btnVoltar;
    private ArrayList<ApoioCamera> lista;
    private final int TIRAR_FOTO = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        imgComparacao = (ImageView) findViewById(R.id.ivComparacao);
        imgImagem = (ImageView) findViewById(R.id.ivImagem);

        btnTrocar = (ImageButton) findViewById(R.id.imageButton6);
        btnTrocar.setOnClickListener(this);

        btnVoltar = (ImageButton) findViewById(R.id.imageButton5);
        btnVoltar.setOnClickListener(this);

        Button foto = (Button) findViewById(R.id.btnFoto);
        foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                takePictureIntent.putExtra("android.takePictureIntent.extras.CAMERA_FACING", 1);
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(takePictureIntent, TIRAR_FOTO);
                }
            }
        });

        //carregarElementos();
       // iniciarJogo();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TIRAR_FOTO && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            imgComparacao.setImageBitmap(imageBitmap);
        }
    }

    public void carregarElementos() {
        lista = new ArrayList<ApoioCamera>();

        ApoioCamera a = new ApoioCamera();
        a.setImagemEmocoes(R.drawable.feliz);
        a.setTextoEmocoes("FELIZ");

        ApoioCamera b = new ApoioCamera();
        b.setImagemEmocoes(R.drawable.feliz2);
        b.setTextoEmocoes("FELIZ");

        ApoioCamera c = new ApoioCamera();
        c.setImagemEmocoes(R.drawable.piscando);
        c.setTextoEmocoes("PISCAR");

        ApoioCamera d = new ApoioCamera();
        d.setImagemEmocoes(R.drawable.desesperado);
        d.setTextoEmocoes("DESEPERAR");

        ApoioCamera e = new ApoioCamera();
        e.setImagemEmocoes(R.drawable.chorando);
        e.setTextoEmocoes("CHORAR");

        ApoioCamera f = new ApoioCamera();
        f.setImagemEmocoes(R.drawable.cetico);
        f.setTextoEmocoes("CÉTICO");

        ApoioCamera g = new ApoioCamera();
        g.setImagemEmocoes(R.drawable.beijos);
        g.setTextoEmocoes("BEIJOS");
    }

    public void iniciarJogo() {
        Collections.shuffle(lista); /* para embaralhar a lista */
        imgImagem.setImageResource(lista.get(1).getImagemEmocoes()); /* setar imagem */
    }


    @Override
    public void onClick(View view) {
        if (view == btnTrocar) {
            iniciarJogo();
        }
        if (view == btnVoltar) {
            Intent i = new Intent(this, Principal.class);
            startActivity(i);
        }
    }
}