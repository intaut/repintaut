package com.example.lab.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Portugues extends AppCompatActivity implements View.OnClickListener {
    private ImageView img;
    private Button btn1, btn2, btn3;
    private ImageButton btnTrocar, port;
    private ArrayList<ApoioPortugues> lista;
    private int itemCerto;
   // private int posicaoantiga;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_portugues);

        img = (ImageView) findViewById(R.id.imagem);

        btnTrocar = (ImageButton) findViewById(R.id.imageButton4);
        btnTrocar.setOnClickListener(this);

        btn1 = (Button) findViewById(R.id.button7);
        btn1.setOnClickListener(this);

        btn2 = (Button) findViewById(R.id.button8);
        btn2.setOnClickListener(this);

        btn3 = (Button) findViewById(R.id.button9);
        btn3.setOnClickListener(this);

        port = (ImageButton) findViewById(R.id.port);
        port.setOnClickListener(this);

        carregarElementos();

        iniciarJogo();
    }

    public void iniciarJogo(){

       btn1.setBackgroundColor(Color.parseColor("#7cb342"));
        btn2.setBackgroundColor(Color.parseColor("#7cb342"));
        btn3.setBackgroundColor(Color.parseColor("#7cb342"));

        Collections.shuffle(lista); /* para embaralhar a lista */

        img.setImageResource(lista.get(0).getImagemPortugues()); /* setar imagem */

        Random r = new Random();
        int posicao = r.nextInt(3); /* gerar inteiro até 3 */

        switch(posicao ){
            case 0:
                itemCerto = 1;
            btn1.setText(lista.get(0).getTextoImagemPortugues());
            btn2.setText(lista.get(1).getTextoImagemPortugues());
            btn3.setText(lista.get(2).getTextoImagemPortugues());
            break;

            case 1:
                itemCerto = 2;
            btn2.setText(lista.get(0).getTextoImagemPortugues());
            btn1.setText(lista.get(1).getTextoImagemPortugues());
            btn3.setText(lista.get(2).getTextoImagemPortugues());
            break;

            case 2:
                itemCerto = 3;
            btn3.setText(lista.get(0).getTextoImagemPortugues());
            btn2.setText(lista.get(1).getTextoImagemPortugues());
            btn1.setText(lista.get(2).getTextoImagemPortugues());
            break;
        }

    }

    public void iniciarAlfabetizacao() {
        int posicao;

        Random r = new Random();
        posicao = r.nextInt(2); // gerar inteiro até 2  0,1

        switch (posicao) {
            case 0: //Portugues
                Intent i = new Intent(this, Portugues.class);
                startActivity(i);
                break;

            case 1: //Matematica
                Intent a = new Intent(this, Matematica.class);
                startActivity(a);
                break;

        }
    }

    public void carregarElementos() {
        lista = new ArrayList<ApoioPortugues>();
        ApoioPortugues a = new ApoioPortugues();
        a.setImagemPortugues(R.drawable.letraa);
        a.setTextoImagemPortugues("A");
        lista.add(a);

        ApoioPortugues b = new ApoioPortugues();
        b.setImagemPortugues(R.drawable.letrab);
        b.setTextoImagemPortugues("B");
        lista.add(b);

        ApoioPortugues c = new ApoioPortugues();
        c.setImagemPortugues(R.drawable.letrac);
        c.setTextoImagemPortugues("C");
        lista.add(c);

        ApoioPortugues d = new ApoioPortugues();
        d.setImagemPortugues(R.drawable.letrad);
        d.setTextoImagemPortugues("D");
        lista.add(d);

        ApoioPortugues e = new ApoioPortugues();
        e.setImagemPortugues(R.drawable.letrae);
        e.setTextoImagemPortugues("E");
        lista.add(e);

        ApoioPortugues f = new ApoioPortugues();
        f.setImagemPortugues(R.drawable.letraf);
        f.setTextoImagemPortugues("F");
        lista.add(f);

        ApoioPortugues g = new ApoioPortugues();
        g.setImagemPortugues(R.drawable.letrag);
        g.setTextoImagemPortugues("G");
        lista.add(g);

        ApoioPortugues h = new ApoioPortugues();
        h.setImagemPortugues(R.drawable.letrah);
        h.setTextoImagemPortugues("H");
        lista.add(h);

        ApoioPortugues i = new ApoioPortugues();
        i.setImagemPortugues(R.drawable.letrai);
        i.setTextoImagemPortugues("I");
        lista.add(i);

        ApoioPortugues j = new ApoioPortugues();
        j.setImagemPortugues(R.drawable.letraj);
        j.setTextoImagemPortugues("J");
        lista.add(j);

        ApoioPortugues k = new ApoioPortugues();
        k.setImagemPortugues(R.drawable.letrak);
        k.setTextoImagemPortugues("K");
        lista.add(k);

        ApoioPortugues l = new ApoioPortugues();
        l.setImagemPortugues(R.drawable.letral);
        l.setTextoImagemPortugues("L");
        lista.add(l);

        ApoioPortugues m = new ApoioPortugues();
        m.setImagemPortugues(R.drawable.letram);
        m.setTextoImagemPortugues("M");
        lista.add(m);

        ApoioPortugues n = new ApoioPortugues();
        n.setImagemPortugues(R.drawable.letran);
        n.setTextoImagemPortugues("N");
        lista.add(n);

        ApoioPortugues o = new ApoioPortugues();
        o.setImagemPortugues(R.drawable.letrao);
        o.setTextoImagemPortugues("O");
        lista.add(o);

        ApoioPortugues p = new ApoioPortugues();
        p.setImagemPortugues(R.drawable.letrap);
        p.setTextoImagemPortugues("P");
        lista.add(p);

        ApoioPortugues q = new ApoioPortugues();
        q.setImagemPortugues(R.drawable.letraq);
        q.setTextoImagemPortugues("Q");
        lista.add(q);

        ApoioPortugues r = new ApoioPortugues();
        r.setImagemPortugues(R.drawable.letrar);
        r.setTextoImagemPortugues("R");
        lista.add(r);

        ApoioPortugues s = new ApoioPortugues();
        s.setImagemPortugues(R.drawable.letras);
        s.setTextoImagemPortugues("S");
        lista.add(s);

        ApoioPortugues t = new ApoioPortugues();
        t.setImagemPortugues(R.drawable.letrat);
        t.setTextoImagemPortugues("T");
        lista.add(t);

        ApoioPortugues u = new ApoioPortugues();
        u.setImagemPortugues(R.drawable.letrau);
        u.setTextoImagemPortugues("U");
        lista.add(u);

        ApoioPortugues v = new ApoioPortugues();
        v.setImagemPortugues(R.drawable.letrav);
        v.setTextoImagemPortugues("V");
        lista.add(v);

        ApoioPortugues x = new ApoioPortugues();
        x.setImagemPortugues(R.drawable.letrax);
        x.setTextoImagemPortugues("X");
        lista.add(x);

        ApoioPortugues z = new ApoioPortugues();
        z.setImagemPortugues(R.drawable.letraz);
        z.setTextoImagemPortugues("Z");
        lista.add(z);
    }


    @Override
        public void onClick(View view) {
            if(view == btnTrocar){
                iniciarAlfabetizacao();
            }
        if(view == port){
            Intent i = new Intent(this, Principal.class);
            startActivity(i);
        }
            else if(view == btn1){
                if(itemCerto == 1){
                    btn1.setBackgroundColor(Color.GREEN);
                    iniciarAlfabetizacao();
                }
                else{
                    btn1.setBackgroundColor(Color.RED);
                }
            }
           else if(view == btn2){
                if(itemCerto == 2){
                    btn2.setBackgroundColor(Color.GREEN);
                    iniciarAlfabetizacao();
                }
                else{
                    btn2.setBackgroundColor(Color.RED);
                }
            }
           else if(view == btn3){
                if(itemCerto == 3){
                    btn3.setBackgroundColor(Color.GREEN);
                    iniciarAlfabetizacao();
                }
                else{
                    btn3.setBackgroundColor(Color.RED);
                }
            }
        }
    }
