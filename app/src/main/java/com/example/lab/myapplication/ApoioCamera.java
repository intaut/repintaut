package com.example.lab.myapplication;

public class ApoioCamera{
    private int imagemEmocoes;
    private String textoEmocoes;

    public int getImagemEmocoes() {

        return imagemEmocoes;
    }

    public void setImagemEmocoes(int imagemEmocoes) {

        this.imagemEmocoes = imagemEmocoes;
    }

    public String getTextoEmocoes() {

        return textoEmocoes;
    }

    public void setTextoEmocoes(String textoEmocoes) {
        this.textoEmocoes = textoEmocoes;
    }
}

