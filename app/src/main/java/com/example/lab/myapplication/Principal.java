package com.example.lab.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import java.util.Random;

public class Principal extends AppCompatActivity implements View.OnClickListener {

    private ImageButton imgButton;
    private Button btn1, btn2, btn3, btn4;
   // private int posicaoantiga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        imgButton = (ImageButton) findViewById(R.id.imageButton);
        imgButton.setOnClickListener(this);
        btn1 = (Button) findViewById(R.id.button1);
        btn1.setOnClickListener(this);
        btn2 = (Button) findViewById(R.id.button2);
        btn2.setOnClickListener(this);
        btn3 = (Button) findViewById(R.id.button3);
        btn3.setOnClickListener(this);
        btn4 = (Button) findViewById(R.id.button4);
        btn4.setOnClickListener(this);
    }

    public void iniciarLabirinto() {
        int posicao;

        Random r = new Random();
        posicao = r.nextInt(4); // gerar inteiro até 4  0,1,2,3

        switch (posicao) {
            case 0: //Labirinto
                Intent i = new Intent(this, Labirinto.class);
                startActivity(i);
                break;

            case 1: //Labirinto2
                Intent a = new Intent(this, Labirinto2.class);
                startActivity(a);
                break;

            case 2://Labirinto3
                Intent b = new Intent(this, Labirinto3.class);
                startActivity(b);
                break;

            case 3://Labirinto4
                Intent c = new Intent(this, Labirinto4.class);
                startActivity(c);
                break;
        }
    }

    public void iniciarAlfabetizacao() {
        int posicao;

        Random r = new Random();
        posicao = r.nextInt(2); // gerar inteiro até 2  0,1

        switch (posicao) {
            case 0: //Portugues
                Intent i = new Intent(this, Portugues.class);
                startActivity(i);
                break;

            case 1: //Matematica
                Intent a = new Intent(this, Matematica.class);
                startActivity(a);
                break;

        }
    }


    @Override
    public void onClick(View view) {
        if (view == imgButton) {
            Intent i = new Intent(this, Inicial.class);
            startActivity(i);
        }
        //Redirecionar LABIRINTO
        if (view == btn1) {
            iniciarLabirinto();

        }
        //Redirecionar IMITACAO
        if (view == btn2) {
            Intent i = new Intent(this, Camera.class);
            startActivity(i);
        }

        //Redirecionar MEMORIA
        if (view == btn3) {


        }
        //Redirecionar ALFABETIZAÇÃO
        if (view == btn4) {
          iniciarAlfabetizacao();


        }
    }
}
