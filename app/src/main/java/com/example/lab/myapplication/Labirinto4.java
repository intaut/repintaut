package com.example.lab.myapplication;

import android.content.ClipData;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import java.util.Random;

public class Labirinto4 extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgcestalegumes, imglaranja;
    private ImageButton imgTrocar, lab4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_labirinto4);

        imgcestalegumes = (ImageView) findViewById(R.id.imgcestalegumes);
        imglaranja = (ImageView) findViewById(R.id.imglaranja);

        imglaranja.setOnLongClickListener(longClickListener);
        imgcestalegumes.setOnDragListener(dragListener);

        imgTrocar = (ImageButton) findViewById(R.id.imageButton7);
        imgTrocar.setOnClickListener(this);

        lab4 = (ImageButton) findViewById(R.id.lab4);
        lab4.setOnClickListener(this);
    }

    public void iniciarLabirinto() {
        int posicao;

        Random r = new Random();
        posicao = r.nextInt(3); // gerar inteiro até 4  0,1,2,3

        switch (posicao) {
            case 0: //Labirinto
                Intent a = new Intent(this, Labirinto.class);
                startActivity(a);
                break;

            case 1://Labirinto2
                Intent b = new Intent(this, Labirinto2.class);
                startActivity(b);
                break;

            case 2://Labirinto3
                Intent c = new Intent(this, Labirinto3.class);
                startActivity(c);
                break;
        }
    }

    View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            ClipData data = ClipData.newPlainText("", "");
            View.DragShadowBuilder myShadowBuilder = new View.DragShadowBuilder(v);
            v.startDrag(data, myShadowBuilder, v, 0);

            return true;
        }
    };


    View.OnDragListener dragListener = new View.OnDragListener(){

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int dragEvent = event.getAction();
            switch (dragEvent){
                case DragEvent.ACTION_DRAG_ENTERED:
                    final View view = (View) event.getLocalState();
                    if (view.getId() == R.id.imglaranja){
                        iniciarLabirinto();
                    }
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    break;
                case DragEvent.ACTION_DROP:
                    break;
            }
            return true;
        }
    };

    @Override
    public void onClick(View view) {
        if(view == imgTrocar){
           iniciarLabirinto();
        }
        if(view == lab4){
            Intent i = new Intent(this, Principal.class);
            startActivity(i);
        }
    }
}