package com.example.lab.myapplication;

import android.content.ClipData;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class Labirinto extends AppCompatActivity implements View.OnClickListener {

    private ImageView imgpapel, imglixo;
    private ImageButton imgTrocar2, lab1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_labirinto);

        imgpapel = (ImageView) findViewById(R.id.imgpapel);
        imglixo = (ImageView) findViewById(R.id.imglixo);

        imgpapel.setOnLongClickListener(longClickListener);
        imglixo.setOnDragListener(dragListener);

        imgTrocar2 = (ImageButton) findViewById(R.id.imgTrocar2);
        imgTrocar2.setOnClickListener(this);

        lab1 = (ImageButton) findViewById(R.id.lab1);
        lab1.setOnClickListener(this);

    }

    public void iniciarLabirinto() {
        int posicao;

        Random r = new Random();
        posicao = r.nextInt(3); // gerar inteiro até 4  0,1,2,3

        switch (posicao) {
            case 0: //Labirinto2
                Intent a = new Intent(this, Labirinto2.class);
                startActivity(a);
                break;

            case 1://Labirinto3
                Intent b = new Intent(this, Labirinto3.class);
                startActivity(b);
                break;

            case 2://Labirinto4
                Intent c = new Intent(this, Labirinto4.class);
                startActivity(c);
                break;
        }
    }


    View.OnLongClickListener longClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            ClipData data = ClipData.newPlainText("", "");
            View.DragShadowBuilder myShadowBuilder = new View.DragShadowBuilder(v);
            v.startDrag(data, myShadowBuilder, v, 0);

            return true;
        }
    };


    View.OnDragListener dragListener = new View.OnDragListener(){

        @Override
        public boolean onDrag(View v, DragEvent event) {
            int dragEvent = event.getAction();
            switch (dragEvent){
                case DragEvent.ACTION_DRAG_ENTERED:
                    final View view = (View) event.getLocalState();
                    if (view.getId() == R.id.imgpapel){
                       iniciarLabirinto();
                    }
                    break;
                case DragEvent.ACTION_DRAG_EXITED:
                    break;
                case DragEvent.ACTION_DROP:
                    break;
            }
            return true;
        }
    };

    @Override
    public void onClick(View view) {
        if(view == imgTrocar2){
           iniciarLabirinto();
        }

        if(view == lab1){
            Intent i = new Intent(this, Principal.class);
            startActivity(i);
        }
    }
}
